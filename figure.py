import os
import numpy as np
from numpy import *

from detector import rapid as r
plots = r.carac.plots

recompall = False
_recomp = {}
ext = ".pdf"
gdir = "./"

try:
    data1
except:
    data1 = None
try:
    data2
except:
    data2 = None
try:
    cube
except:
    cube = None
try:
    darks
except:
    darks = None
try:
    data3
except:
    data3 = None

try:
    ptcs
except:
    ptcs = None




output = 6

section1 = r.rapid(output, None)
section2 = r.rapid(4, (10,40), (0,0))
signalrange = (0,4000)

####
# some weird pixels
# 120,100   -> noise picks
# 40, 309   -> unstable
#  (140, 62) -> unstable
#  (238, 136) -> huuuuge variation
#  (110, 251) -> unstable (same output as grism)
#  (58, 248) -> normal
# ptc = data1.withDit(0.001).getDetCarac(signalrange=(0,4000))
# chi2 = ptc.getPtcchi2()
# ou = np.where( (chi2.A>2000) * (chi2.A<2500) * (ptc.getY().A>30) * (ptc.getX().A>30) )
###

def get_data1(polar=7100):
    global data1
    if data1 is None:

        def filter(filename, opener):
            f = opener(filename)
            return f[0].header["HIERARCH ESO DET POLAR"]>7000, f


        fls = r.carac.Carac(r.local.open("*FLAT*.fits", "Pionier3d",
                            opener=r.carac.Combined.open#, ffilter=filter
                            ))
        #fls = fls.withPolar(7100)
        ####
        # substract the dark (illumination = -10) to each files DIT:
        for fp in fls.iterPolar():
            for f in fp.iterDit():
                dark = f.withPolar(f.getPolar()[0]).withIllumination(-10)[0]
                print "substract dark: ", dark.getDit(), dark.getIllumination()
                f.setOffset(-dark.getSignal(nooffset=True))

        flats = fls
        data1 = flats

    return data1.withPolar(polar)

def get_data2():
    global data2
    if data2 is None:
        def filter(filename, opener):
            f = opener(filename)
            return f[0].header["HIERARCH ESO DET POLAR"]>7000, f
        all_dark_list = r.carac.Carac(r.local.open("PIONIER_DARK*_02[0-9][0-9].fits", "2014-12-08", opener=r.carac.Combined.open,ffilter=filter))
        all_bias_list = r.carac.Carac(r.local.open("PIONIER_BIAS343*0012.fits", "2014-12-08", opener=r.carac.Combined.open,ffilter=filter))

        polars = all_dark_list.getPolarSet()
        darks = []
        for p in polars:
            ds = all_dark_list.withPolar(p)
            bias  = all_bias_list.withPolar(p)[0]
            ds.setOffset( -bias.getSignal(nooffset=True) )
        data2 = all_dark_list.withPolar(7100)
    return data2


def get_data_darks():
    global darks
    if darks is None:
        darks = r.carac.Carac(r.local.open("*[1-9][0-9].fits", "DarkSeri",
                                opener=r.carac.Combined.open)[2:])


        offset = darks.getSignal(section=r.grism(win=1), nooffset=True,
                             expo_reduce=median
                             )

        for d in darks:
            sub = median(d.getSignal(noofset=True,
                         section=r.grism(win=1))-offset)
            print sub
            d.setOffset(-sub)

        #darks = r.carac.Carac(r.local.open("*[0-9].fits", "DarkSeri",
        #                        opener=r.carac.Combined.open))


    return darks


def get_data_cube():
    global cube
    if cube is None:
        cube = r.local.open( "Paranal/PIONIER_GEN_PTC_0F347_0002.fits", opener=r.carac.Cube.open)[0]
    return cube

def get_data3():
    global data3

    def ffilter(filename, opener):
            f = opener(filename)
            return f[0].header["HIERARCH ESO DET POLAR"]>7000, f

    if data3 is None:
        all_dark_list = r.carac.Carac(r.local.open( "PIONIER_DARK*.fits", "2014-12-08", opener=r.carac.Combined.open, ffilter=ffilter))
        all_bias_list = r.carac.Carac(r.local.open( "PIONIER_BIAS*.fits", "2014-12-08", opener=r.carac.Combined.open, ffilter=ffilter))

        polars = all_dark_list.getPolarSet()
        darks = []
        for p in polars:
            ds = all_dark_list.withPolar(p)
            bias  = all_bias_list.withPolar(p)[0]
            ds.setOffset( -bias.getSignal(nooffset=True) )
        data3 = all_dark_list
    return data3


def get_ptcs():
    global ptcs
    if ptcs is None:
        ptcs = r.carac.ptc.PtcLinearityDataList( r.local.open( "2014-12-08/ptc_p*.fits", opener=r.carac.ptc.PtcLinearityHDUList.open))
    return ptcs

def path(name):
    return gdir+name+ext

def mayrecomp(name,force=False):
    return force or recompall or (name in _recomp) or (not os.path.exists(path(name)))


def plotdec(pf, name, size=(0.5,0.35)):
    def tmp(force=False, size=size, **kwargs):

        if mayrecomp(name, force):
            print "<PYTHON> Ploting Figure :", name
            fig = plots.figure(name)
            fig.clear()
            pf(name, **kwargs)
            fig.set_size_inches( s*7.5 for s in size )
            for ax in fig.axes:
                for tick in ax.yaxis.get_major_ticks():
                    tick.label.set_fontsize(8)
                for tick in ax.xaxis.get_major_ticks():
                    tick.label.set_fontsize(8)
                ax.xaxis.label.set_fontsize(10)
                ax.yaxis.label.set_fontsize(10)
            if name not in ["bad_pixcube"]:
                fig.tight_layout()
            fig.savefig(path(name))

        return path(name)
    return tmp


def _gain_distrib(name, dit=0.001):

    bins = linspace(0.0, 1.5, 150)

    ca = get_data1(7100).withDit(dit).getPtc(signalrange=signalrange)

    im = ca.Gain().plots.img(section=section1,
                             figure=name)

    im.hist(bins=bins, color="white", label="p = 7.1v")




    ca = get_data1(5000).withDit(dit).getPtc(signalrange=signalrange)

    im = ca.Gain().plots.img(section=section1,
                             figure=name)

    im.hist(bins=bins, color="green", alpha=0.5, label="p = 5.0v")

    ca = get_data1(3000).withDit(dit).getPtc(signalrange=signalrange)

    im = ca.Gain().plots.img(section=section1,
                             figure=name)



    im.hist(bins=bins, color="blue", alpha=0.5, label="p = 3.0v")
    im.legend(prop={"size": 8})





    im.axes_set(xlabel="Gain [e-/ADU]", ylabel="Histogram",
                xlim=(0,1.5))
gain_distrib = plotdec(_gain_distrib, "gain_distrib")

def gain_value(section=section1, dit=0.001, freduce=median, fmt="%4.2f"):

    ca3 = get_data1(3000).withDit(dit).getPtc(signalrange=signalrange)
    ca3_2 = get_data1(3000).withIllumination(8).getDetCarac(signalrange=signalrange)

    ca7 = get_data1(7100).withDit(dit).getPtc(signalrange=signalrange)

    ca7_2 = get_data1(7100).withIllumination(8).getDetCarac(signalrange=signalrange)

    M7 = get_data1(7100).withDit(0.002).withIllumination(8)[0].getSignal(section=section) /get_data1(3000).withDit(0.002).withIllumination(8)[0].getSignal(section=section)
    mM7 = freduce(M7)

    M7_2 = ca7_2.getFlux(section=section)/ca3_2.getFlux(section=section)
    mM7_2 = freduce(M7_2)


    F7 = ca3.getGain(section=section)/ca7.getGain(section=section)#*1.0/M7
    mF7 = freduce(F7)

    print 7.1, fmt%ca7.getGain(section=section, freduce=freduce), fmt%mM7, fmt%mF7, fmt%mM7_2
    ca5 = get_data1(5000).withDit(dit).getPtc(signalrange=signalrange)



    M5 = get_data1(5000).withDit(0.002).withIllumination(8)[0].getSignal(section=section) /get_data1(3000).withDit(0.002).withIllumination(8)[0].getSignal(section=section)
    mM5 = freduce(M5)
    F5 = ca3.getGain(section=section)/ca5.getGain(section=section)*1.0/M5
    mF5 = freduce(F5)

    print 5.0, fmt%ca5.getGain(section=section, freduce=freduce), fmt%mM5,  fmt%mF5

    mM3 = 1.0
    mF3 = 1.0
    print 3.0, fmt%ca3.getGain(section=section, freduce=freduce), fmt%mM3, fmt%mF3


def _gain_distrib_map(name):
    ca = get_data1().withDit(0.001).getPtc(signalrange=signalrange)

    im = ca.Gain().plots.img(#section=section1,
                             figure=name)

    im.hist(bins=linspace(0.08, 0.5, 100), color="white", axes=(2,2))

    im.axes_set(xlabel="Gain [e-/ADU]", ylabel="Histogram", axes=(2,2))

    im.imshow(vmin=0.08, vmax=0.5, axes=(2,1))

gain_distrib_map = plotdec(_gain_distrib_map, "gain_distrib_map")




def _noise_distrib(name):
    ca = get_data1().withDit(0.001).getPtc(signalrange=signalrange)
    n2 = ca.getNoise2()
    g = ca.getGain()
    noise = sqrt(n2) * g

    im = plots.ImgPlot.newWithData(img=noise)


    im.imshow(vmin=0, vmax=1, axes=(2,1), origin="lower")
    im.hist(bins=linspace(0,1, 50), axes=(2,2))
    im.axes_set(xlabel="Noise [e- rms]", ylabel="Histogram", axes=(2,2))

noise_distrib = plotdec(_noise_distrib, "noise_distrib")

def _noise_distrib_cut(name, bins=linspace(0,11, 50), signalrange = signalrange,
                       method=1, electron=1, slc=slice(None,None),
                       plotcut=False, section=section1
                       ):


    ca = get_data1().withDit(0.0005).getPtc(signalrange=signalrange)

    g = ca.getGain()

    if method == 1:
        n2 = ca.getNoise2()
        noise = sqrt(n2)
        if not electron:
            noise = noise / g
    else:
        n2 = get_data1().withDit(0.0005).withIllumination(min)[0].getSigma2()
        noise = sqrt(n2)
        if electron:
            noise = noise * g


    noise = noise[section]
    im = plots.ImgPlot.newWithData(img=noise, figure=name)

    haxes = (2,1) if plotcut else (1,1)
    caxes = (2,2) if plotcut else (1,1)


    im.hist(bins=bins, axes=haxes, color="blue", alpha=0.5)
    xlim = (0,12)

    im.axes_set(xlabel="Noise [e- rms]" if electron else "Noise [ADU rms]",
                ylabel="Histogram", axes=haxes,
                xlim=xlim
                )
    gain = 0.13
    if electron:
        axtop = im.axes.twiny()
        adus = [0,10,20,30,40,50,60,70,80,90]
        lock = [(adu*gain)/(xlim[1]-xlim[0]) for adu in adus]
        sadus = ["%d"%adu for adu in adus]

        axtop.set_xticks(array(lock))
        axtop.set_xticklabels(sadus)
        axtop.set_xlabel("Noise [ADU rms]")
        for tick in axtop.xaxis.get_major_ticks():
            tick.label.set_fontsize(8)
        axtop.xaxis.label.set_fontsize(10)


    if plotcut:
        cut  = plots.XYPlot.newWithData(x=arange(noise.shape[0]),
                                        y=median(noise.A[:,slc], axis=1),
                                        color="blue", #marker="-",
                                        figure=name, axes=(2,2)
                                        )
        cut.plot()
        cut.axes_set(xlabel="Y Pixel",
                     ylabel="Noise [e- rms]" if electron else "Noise [ADU rms]"
                     )


    ################
    ca = get_data1().withDit(0.002).getPtc(signalrange=signalrange)
    g2 = ca.getGain()

    if method == 1:
        n2 = ca.getNoise2()
        noise = sqrt(n2)
        if not electron:
            noise = noise / g2
    else:
        n2 = get_data1().withDit(0.002).withIllumination(min)[0].getSigma2()
        noise = sqrt(n2)
        if electron:
            noise = noise * g2


    noise = noise[section]
    im = plots.ImgPlot.newWithData(img=noise, figure=name)

    im.hist(bins=bins, axes=haxes, color="red", alpha=0.5)

    if plotcut:
        cut  = plots.XYPlot.newWithData(x=arange(noise.shape[0]),
                                        y=median(noise.A[:,slc], axis=1),
                                        color="red", linestyle="--",
                                        figure=name, axes=(2,2)
                                        )
        cut.plot()
    ###################################

    n2 = get_data1().withDit(min).withIllumination(min)[0].getSigma2()
    noise = sqrt(n2)
    if electron:
        noise *= g

    noise = noise[section]
    print "SHAPE", noise.shape
    im = plots.ImgPlot.newWithData(img=noise, figure=name)

    im.hist(bins=bins, axes=haxes, color="black", alpha=0.5)


    if plotcut:
        cut  = plots.XYPlot.newWithData(x=arange(noise.shape[0]),
                                        y=median(noise.A[:,slc], axis=1),
                                        color="black", linestyle="-",
                                        figure=name, axes=(2,2)
                                        )
        cut.plot()



noise_distrib_cut = plotdec(_noise_distrib_cut, "noise_distrib_cut")





def _flat(name):
    ca = get_data1().withIllumination(6).withDit(0.001)[0].Signal()
    im = ca.plots.img(figure=name)

    #im.hist(bins=linspace(0, 5000, 100), color="white")
    im.imshow(vmin=100, vmax=1500, cmap="gray", origin="lower")
    for i in range(8):
        #plots.Rectangle(*r.rapid(i).rectangle()[0], color="green", axes=im.axes)
        im.axes.add_patch( plots.Rectangle(*r.rapid(i).rectangle()[0],
                          color="green", fill=False))
    im.axes_set(xlabel="X", ylabel="Y")
flat = plotdec(_flat, "flat")

def _ptc_example(name, section=r.rapid(4,1), signalrange=signalrange):
    ca = get_data1().withDit(0.001).withIllumination((-10,8))
    ptc= ca.plots.ptc(section=section, xrange=signalrange)
    ptc.all(nolegend=True)
ptc_example = plotdec(_ptc_example, "ptc_example")



def _flux_map(name, section=section1, windows=False):
    ca = get_data2().withPolar(7100)
    lin = ca.getDetCarac(signalrange=signalrange)

    fig = plots.figure(name)
    ax1 = plots.subplot2grid((1,5), (0, 0), colspan=4)
    ax2 = plots.subplot2grid((1,5), (0, 4), colspan=1)

    im = lin.plots.eflux(figure=name)
    im.imshow(vmin=0, vmax=10, cmap="gray", origin="lower", axes=ax1)
    im.axes_set(title="")
    im.colorbar(cmap="gray").set_label(label="Flux [e-/ms]")

    ax1.add_patch(plots.Rectangle(*section.rectangle()[0],
                  color="green", fill=False))
    yo = []

    if windows:
        for rec in r.grism().rectangle():
            yo.append( rec[0][1] )
            ax1.add_patch(plots.Rectangle(*rec,
                          color="blue", fill=False))


    flux = lin.getEflux(section=section, x_reduce=median)/1000.
    rms  = lin.getEflux(section=section, x_reduce=std)/1000.

    x = arange(flux.shape[0])
    ft = plots.Fit2d(dim=1)
    print "fit ", ft.fit(x[:230], flux[:230])

    ax2.plot(flux, x , color="black")
    xm = np.linspace(0, flux.shape[0], 2)

    ax2.plot(ft.get_model(xm), xm, color="red", linestyle="--" )
    #ax2.errorbar( flux, x, xerr=rms)
    #ax2.fill(  list(flux+rms)+list(flux-rms)[::-1] , list(x)+list(x)[::-1], color="grey", alpha=0.4)

    if windows:
        ax2.hlines(yo, 2, 3, color="blue")
    ax2.yaxis.tick_right()
    ax2.yaxis.set_label_position("right")
    ax2.set_ylabel("Y")
    ax2.set_xlabel("flux [e-/ms]")
    ax2.set_ylim(0, flux.shape[0])
    #ax2.set_xlim(0, 12)
flux_map = plotdec(_flux_map, "flux_map")




def _bad_pixcube(name, instable=(110, 251), noisy=(120,100), normal=r.grism(pixel=7),
             dy=100, dx=10
             ):
    ax1 = plots.figure(name).add_subplot(211)
    ax2 = plots.figure(name).add_subplot(212, sharex=ax1)
    #ax3 = plots.figure(name).add_subplot(313, sharex=ax2)

    cb = get_data_cube()[0]
    cube = cb.data
    dark = get_data1().withDit(cb.getDit()).withIllumination(-10)[0].getSignal(nooffset=True)

    ptc = get_data1().withDit(cb.getDit()).getPtc(signalrange=signalrange)
    f_instable = cube[(slice(0,None),)+instable]-dark[instable]
    #f_noisy  = cube[(slice(0,None),)+noisy ]-dark[noisy]
    f_normal = cube[(slice(0,None),)+normal]-dark[normal]

    print median(f_normal), "gain: ", ptc.getGain(section=normal)
    print median(f_instable), "gain: ", ptc.getGain(section=instable)

    #f_instable -= median(f_instable)
    #f_noisy  = f_noisy
    #f_normal -= median(f_normal)

    ax1.plot(f_normal, color="black")
    #ax2.plot(f_noisy, color="red")
    ax2.plot(f_instable-median(f_instable)+30, color="red")

    ax2.set( xlabel="frame Number",
            ylabel="Signal[ADU]",
            ylim=(-20, 150), xlim=(-100,4200)
            )
    ax1.set( #ylabel="Signal[ADU]",
             ylim=(-20, 150), xlim=(-100,4200)
            )
    plots.figure(name).subplots_adjust(hspace=0)
bad_pixcube = plotdec(_bad_pixcube, "bad_pixcube")



def _bad_pix(name, instable=(110, 251), noisy=(120,100), normal=r.grism(pixel=7),
             dy=0, dx=10
             ):
    ax1 = plots.figure(name).add_subplot(111)

    darks = get_data_darks()
    mjd = darks.getKey("MJD-OBS")
    mjd -= mjd.min()
    time = mjd*24*3600

    #offset = darks.getMedianSignal(section=r.grism(win=0)) # take the grism pixel has a dark window
    #offset -= median(offset)
    offset = 0

    f_normal = darks.getMeanSignal(section=normal)-offset
    f_instable = darks.getMeanSignal(section=instable)-offset
    f_noisy = darks.getMeanSignal(section=noisy)-offset

    f_normal -= median(f_normal)
    f_instable -= median(f_instable)
    f_noisy -= median(f_noisy)


    ax1.errorbar( time, f_normal, yerr=darks.getMeanSignalerror(section=normal), color="black")
    #ax1.fill_between(time, f_normal-darks.getMeanSignalerror(section=normal)*2,
    #                 f_normal+darks.getMeanSignalerror(section=normal)*2,
    #                 color="gray", alpha=0.5)
    #ax2.errorbar( time, darks.getSignal(section=noisy), yerr=darks.getSigma(section=noisy))
    #ax3.errorbar( time, darks.getSignal(section=instable), yerr=darks.getSigma(section=instable))
    ax1.errorbar( time+dx,  f_instable+dy, yerr=darks.getMeanSignalerror(section=instable), color="red")
    #ax1.errorbar( time+dx*2, f_noisy+dy*2, yerr=darks.getSigma(section=noisy), color="green")

    ax1.set( xlabel="Time [s]", ylabel="Nomalized Signal [ADU]")


bad_pix = plotdec(_bad_pix, "bad_pix")


def _pic2pic_varia(name,df=17,lf=8, bins=linspace(0,30,50)):
    darks = get_data_darks()

    def deviation(data,axis=None):
        return data.max(axis=axis)-data.min(axis=axis)


    pl = plots.ImgPlot.newWithData(img=darks.getSignal(expo_reduce=deviation),
                                   figure=name)
    pl.hist(bins=bins, color="white")
    pl.axes_set(xlabel="Pic to pic variation [ADU]",
                ylabel="Histogram"
                )

    pl = plots.ImgPlot.newWithData(img=darks.getSignal(expo_reduce=deviation,
                                   expo_idx=slice(df,df+lf)),
                                   figure=name)
    pl.hist(bins=bins, color="blue", alpha=0.2)

    #pl = plots.ImgPlot.newWithData(img=darks.getSignal(expo_reduce=np.std), figure=name)
    #pl.hist(bins=linspace(0,40,50), color="red", alpha=0.2)

    #pl = plots.ImgPlot.newWithData(img=darks.getSignal(expo_reduce=np.std,expo_idx=slice(df,df+lf)), figure=name)
    #pl.hist(bins=linspace(0,40,50), color="green", alpha=0.2)



pic2pic_varia = plotdec(_pic2pic_varia, "pic2pic_varia")


def _gain_polar(name, dim=2, xrange=(2, None), section=section2, polar_unit="v"):

    ptcs = get_ptcs()

    ptcs.plots.gainpolar(freduce=np.nanmedian, polar_unit=polar_unit,
                         section=section, figure=name).all(xrange=xrange,
                         nolegend=True,
                         dim=dim)

gain_polar = plotdec(_gain_polar, "gain_polar")

def _gainm_polar(name, dim=3, xrange=(None, None), section=section2, polar_unit="v"):

    ptcs = get_ptcs()

    ptcs.plots.gainmpolar(freduce=np.nanmedian,
                          section=section, figure=name,
                          polar_unit=polar_unit).all(xrange=xrange, dim=dim,
                                                nolegend=True,
                                                ylabel="Multiplicative Gain")

gainm_polar = plotdec(_gainm_polar, "gainm_polar")

def _chi2_linearity(name, ditrange=(0.0005,None), section=r.rapid(4,40,(100,0))):
    #data = get_data1(7100).withIllumination(9)
    data = get_data3().withPolar(7100)
    #lin4 = data.getLinearity( ditrange=(0.0005, None), signalrange=(0, 4000))
    #lin5 = data.getLinearity( ditrange=(0.0005, None), signalrange=(0, 5000))
    #lin6 = data.getLinearity( ditrange=(0.0005, None), signalrange=(0, 6000))
    #lin7 = data.getLinearity( ditrange=(0.0005, None), signalrange=(0, 7000))

    bins = linspace(0,50,50)
    i=1
    for smax,color in zip([2000,3000,4000,5000], ["blue","blue","green","red"]):
        lin = data.getLinearity( ditrange=ditrange, signalrange=(0, smax))

        chi2 = plots.ImgPlot.newWithData(img=lin.getLinchi2(section=section),figure=name, axes=(4,1,i))

        #chi2 = lin.Linchi2(section=section).plots.img(figure=name, axes=(4,1,i))
        chi2.hist(bins=bins, color=color, label="<%.0f ADU"%smax, alpha=0.4)
        i += 1
    chi2.axes_set(xlabel="Chi2", ylabel="Histogram")
chi2_linearity = plotdec(_chi2_linearity, "chi2_linearity")


def fig(name):
    return {"gain_distrib":gain_distrib}[name]()
