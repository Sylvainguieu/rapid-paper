#!/anaconda/bin/python
from jinja2 import Template, Environment, FileSystemLoader
import figure
reload(figure)
from figure import gain_distrib, output

import resource
resource.setrlimit(resource.RLIMIT_NOFILE, (1000,-1))

env = Environment(
    "%<", ">%",
    "<<", ">>",
    "%{#", "#}%",
    loader=FileSystemLoader(".")
)

import os
file_in = "rapid.tex"
file_out = "tmp.tex"
ile_out = "tmp.tex"
graph_dir = "."
graph_ext = ".pdf"

from detector import rapid as r

def ptc_data():
    pass

def toto():
    return 45


def compile(recompall=False):
    figure.recompall = False
    figure._recomp = []

    if hasattr(recompall, "__iter__"):
        figure._recomp = recompall
    else:
        figure.recompall = recompall


    if not os.path.exists(graph_dir):
        os.makedirs(graph_dir)

    with open(file_in, "r") as template:
        template = env.get_template(file_in)

    with open(file_out, "w") as latex:
        latex.write(template.render(**globals()))
    os.system("pdflatex " + file_in.replace(".tex",""))
    os.system("bibtex " + file_in.replace(".tex",""))
    os.system("pdflatex " + file_in.replace(".tex",""))


if __name__ == "__main__":
    compile()
